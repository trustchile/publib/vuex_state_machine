import Axios, {
  CancelTokenSource,
  AxiosInstance,
  AxiosTransformer,
  Method
} from 'axios';
import { VUEX_DEEP_SET as VueDeepSet } from 'vue-deepset';
import getProperty from 'lodash/get';
import moment, { Moment } from 'moment';

export type Status = 'idle' | 'pending' | 'fulfilled' | 'rejected';

export interface Filter {
  limit: number | null;
  order: 'asc' | 'desc' | null;
  search: string | null;
  date: string | null;
}

export interface Pagination {
  itemsPerPage: number;
  currentPage: number;
  totalPages: number;
  totalItems: number;
}

export interface Payload {
  path: string;
  value: unknown;
}

export interface HTTP {
  request: CancelTokenSource | null;
  client: AxiosInstance;
}

export interface Context<D> {
  machineName: string;
  status: Status;
  integrity: ContextIntegrity;
  reference: ContextReference;
  backendPaginated: boolean;
  backendFilterable: boolean;
  http: HTTP;
  initialData: D;
  filter: Filter;
  pagination: Pagination;
  createdAt: Moment;
  updatedAt: Moment;
}

export type HTTPMethod = Method;
export type ContextIntegrity = 'empty' | 'non-empty' | 'unknown';
export type ContextReference = 'clientOnly' | 'both' | 'unknown';

export interface MethodConfig {
  type: HTTPMethod;
  actionName: string;
  sourceProperty?: string;
  path: string | ((state: VuexState) => string);
  transformRequest?: (state: VuexState) => AxiosTransformer;
  transformResponse?: (state: VuexState) => AxiosTransformer;
}

export interface Option<T = any, M = any> {
  propertyName: string;
  initialValue: T;
  injectMetadata?: M;
  backendPaginated?: boolean;
  backendFilterable?: boolean;
  doNotRetryOnReject?: boolean;
  initialStatus?: Status;
  initialIntegrity?: ContextIntegrity;
  initialReference?: ContextReference;
  initialPagination?: Partial<Pagination>;
  initialFilters?: Partial<Filter>;
  methods: MethodConfig[];
}

export interface MachineState<D = any, M = null> {
  data: D;
  metadata: M;
  context: Context<D>;
}

export interface ActionOptions {
  force?: boolean;
}

export type VuexCommit = (
  mutationName: 'set' | 'add' | 'push' | 'reset',
  payload: Payload | Payload[]
) => void;

export type VuexDispatch = (actionName: string, options: ActionOptions) => void;

export interface VuexState {
  [property: string]: MachineState;
}

export interface VuexActions {
  [property: string]: (
    {
      state,
      commit,
      dispatch
    }: { state: VuexState; commit: VuexCommit; dispatch: VuexDispatch },
    options?: ActionOptions
  ) => void;
}

export default class FiniteStateMachine {
  public readonly namespaced: boolean = true;
  public options: Option[] = [];

  constructor(options: Option[]) {
    this.options = options;
  }

  public get state() {
    return () => {
      return this.options.reduce((accumulator, option) => {
        accumulator[option.propertyName] = {
          data: option.initialValue,
          metadata: option.injectMetadata,
          context: {
            status: option.initialStatus ?? 'idle',
            integrity: option.initialIntegrity ?? 'unknown',
            reference: option.initialReference ?? 'unknown',
            machineName: option.propertyName,
            backendPaginated: option.backendPaginated ?? false,
            backendFilterable: option.backendFilterable ?? false,
            initialData: option.initialValue,
            createdAt: moment(),
            updatedAt: moment(),
            filter: {
              limit: null,
              order: null,
              search: null,
              date: null,
              ...(option.initialFilters ?? {})
            },
            pagination: {
              currentPage: 1,
              totalItems: 1,
              totalPages: 1,
              itemsPerPage: 1,
              ...(option.initialPagination ?? {})
            },
            http: {
              client: Axios.create(),
              request: null
            }
          }
        };
        return accumulator;
      }, {} as VuexState);
    };
  }

  public get actions(): VuexActions {
    return this.options.reduce((accumulator, option) => {
      option.methods.forEach((methodConfig) => {
        accumulator[methodConfig.actionName] = async function (
          { state, commit, dispatch },
          actionOptions
        ) {
          const statePath = state[option.propertyName];
          const {
            data: stateData,
            metadata: stateMetadata,
            context: stateContext
          } = statePath;
          const stateStatus = stateContext.status;
          const httpRequest = stateContext.http.request;
          if (
            ((stateStatus === 'idle' ||
              (stateStatus === 'rejected' && !option.doNotRetryOnReject)) &&
              !httpRequest) ||
            actionOptions?.force
          ) {
            commit('set', {
              path: `${option.propertyName}.context.status`,
              value: 'pending'
            });

            /**
             * Si existe una conexion pendiente http la cancelamos y guardamos
             */
            if (statePath.context.http.request) {
              statePath.context.http.request.cancel();
              commit('set', {
                path: `${option.propertyName}.context.http.request`,
                value: null
              });
            }

            /**
             * Las rutas son dinamicas por lo que puede ser un String o una funcion callback.
             * Nosotros debemos pasarle el State para que por el otro lado de la funcion, se pueda calcular la URL
             * en base al state completo.
             */
            const url =
              typeof methodConfig.path === 'string'
                ? methodConfig.path
                : methodConfig.path(state);

            /**
             * Guardamos la request para poder cancelarla despues.
             */
            commit('set', {
              path: `${option.propertyName}.context.http.request`,
              value: Axios.CancelToken.source()
            });

            try {
              /**
               * Ejecutamos la peticion final
               */
              const { data, headers } = await statePath.context.http.client({
                url,
                method: methodConfig.type,
                data:
                  methodConfig.type !== 'GET' && methodConfig.type !== 'DELETE'
                    ? stateData
                    : null,
                params: { ...stateContext.pagination, ...stateContext.filter },
                transformRequest: methodConfig.transformRequest?.(state),
                transformResponse: methodConfig.transformResponse?.(state),
                cancelToken: statePath.context.http.request?.token
              });

              /**
               * Guardamos en la Store la respuesta y seteamos el Status
               */
              commit('set', [
                { path: `${option.propertyName}.data`, value: data },
                { path: `${option.propertyName}.context.status`, value: 'fulfilled' },
                {
                  path: `${option.propertyName}.context.integrity`,
                  value: FiniteStateMachine.CheckIntegrity(data)
                },
                {
                  path: `${option.propertyName}.context.reference`,
                  value:
                    methodConfig.type !== 'GET' &&
                    methodConfig.type !== 'DELETE'
                      ? 'both'
                      : 'clientOnly'
                }
              ]);

              if (statePath.context.backendPaginated) {
                commit('set', [
                  {
                    path: `${option.propertyName}.context.pagination.totalPages`,
                    value: Number(headers['x-total-pages']) ?? 1
                  },
                  {
                    path: `${option.propertyName}.context.pagination.totalItems`,
                    value: Number(headers['x-total-items']) ?? 1
                  },
                  {
                    path: `${option.propertyName}.context.pagination.itemsPerPage`,
                    value: Number(headers['x-items-per-page']) ?? 1
                  },
                  {
                    path: `${option.propertyName}.context.pagination.currentPage`,
                    value: Number(headers['x-current-page']) ?? 1
                  }
                ]);
              }

              if (statePath.context.backendFilterable) {
                commit('set', [
                  {
                    path: `${option.propertyName}.context.filter.limit`,
                    value: Number(headers['x-filter-limit']) ?? -1
                  },
                  {
                    path: `${option.propertyName}.context.filter.order`,
                    value: Number(headers['x-filter-order']) ?? null
                  },
                  {
                    path: `${option.propertyName}.context.filter.search`,
                    value: Number(headers['x-filter-search']) ?? null
                  },
                  {
                    path: `${option.propertyName}.context.filter.date`,
                    value: Number(headers['x-filter-date']) ?? null
                  }
                ]);
              }
            } catch (e) {
              const statusCode = e?.response?.status ?? 0;
              if (statusCode === 401 || statusCode === 403) {
                window.location.href = '/auth';
                return false;
              } else if (!Axios.isCancel(e)) {
                commit('set', {
                  path: `${option.propertyName}.context.status`,
                  value: 'rejected'
                });
                console.error("[VuexStateMachine]", e);
              } else if (process.env.NODE_ENV !== 'production') {
                console.warn("[VuexStateMachine]", e);
              }
            }
          }
        };
      });
      return accumulator;
    }, {} as VuexActions);
  }

  public get mutations(): any {
    return {
      set: this.set,
      add: this.add,
      push: this.push,
      reset: this.reset
    };
  }

  /**
   * Modifica la Store con DeepSet en cualquier path y con cualquier valor. Usar con cuidado.
   * @param state Vuex State
   * @param payload {Payload}
   */
  private set(state: VuexState, payload: Payload | Payload[]) {
    if (Array.isArray(payload)) {
      payload.forEach((item) => {
        FiniteStateMachine.DeepSet(state, item.path, item.value);
      });
    } else {
      FiniteStateMachine.DeepSet(state, payload.path, payload.value);
    }
  }

  private add(state: VuexState, payload: Payload | Payload[]) {
    if (Array.isArray(payload)) {
      payload.forEach((item) => this.push(state, item));
    } else {
      this.push(state, payload);
    }
  }

  private push(state: VuexState, payload: Payload) {
    const data = FiniteStateMachine.GetProperty(
      state,
      payload.path,
      []
    ) as any[];

    const shallowCopy = FiniteStateMachine.Clone<any[]>(data);
    shallowCopy.push(payload.value);
    FiniteStateMachine.DeepSet(state, payload.path, shallowCopy);
  }

  private reset(state: VuexState, path: string) {
    if ('state' in state[path]) {
      this.set(state, [
        {
          path: `${path}.data`,
          value: FiniteStateMachine.Clone(state[path].context.initialData)
        },
        {
          path: `${path}.context.status`,
          value: 'idle'
        }
      ]);
    }
  }

  public static DeepSet(target: unknown, path: string, value: unknown) {
    VueDeepSet(target, { path, value });
  }

  public static GetProperty = getProperty;

  public static Clone<T = any>(object: T): T {
    return JSON.parse(JSON.stringify(object));
  }

  public static CheckIntegrity(data: unknown): ContextIntegrity {
    if (Array.isArray(data)) {
      if (data.length > 0) {
        return 'non-empty';
      } else {
        return 'empty';
      }
    } else if (typeof data === 'object' && data !== null) {
      if (Object.keys(data).length > 0) {
        return 'non-empty';
      } else {
        return 'empty';
      }
    } else {
      return 'unknown';
    }
  }
}
