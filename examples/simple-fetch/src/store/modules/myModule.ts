import VuexStateMachine from '@trusttechnologies/vuex_state_machine'

export const myModule = new VuexStateMachine([
  {
    /** propertyName sera el nombre del 'state' de Vuex */
    propertyName: 'root',
    initialValue: {},
    initialIntegrity: "empty",
    methods: [
      {
        type: 'GET',
        actionName: 'fetchRoot',
        /**
         * path de ejemplo, debes ir y crear el tuyo propio. 
         * Luego, borrar el endpoint (o deshabilitar la respuesta) de mocky y apreciar los resultados
         */
        path: 'https://run.mocky.io/v3/7e7806ff-6e84-4ad5-a3e0-aca6abec3079'
      }
    ]
  }
])
