# Vuex State Machine @ Trust Technologies

Trust plugin para Vuex - Brinda la posibilidad de utilizar maquinas de estado para manejar carga asincronica de datos entre backend y frontend, mantener una estructura de integridad de los mismos asi como tambien paginación y filtro.

## alpha release - wip
Actualmente solo esta cubierto el caso de traer recursos del backend (READ).

```typescript
// @/src/store/modules/my-module.ts
import VuexStateMachine from '@trusttechnologies/vuex_state_machine'

export const myModule = new VuexStateMachine([{
  /** propertyName sera el nombre del 'state' de Vuex */
  propertyName: "root",
  initialValue: [],
  methods: [{
    type: "GET",
    actionName: "fetchRoot",
    path: "/my/module/endpoint",
  }]
}])
```

Lo de arriba crea en la Vuex Store todos los metodos para implementar una maquina de estados, la cual maneja los posibles caminos a recorrer tal cual una `Promise`.

```vue
<template>
  <div>
    <h1>myModule Store</h1>
    <p>data: {{ $store.state.myModule.root.data }}</p>
    <p>metadata: {{ $store.state.myModule.root.metadata }}</p>
    <h2>Context</h2>
    <p>status: {{ $store.state.myModule.context.status }}</p>
    <p>integrity: {{ $store.state.myModule.context.integrity }}</p>
    <p>reference: {{ $store.state.myModule.context.reference }}</p>
    <hr/>
    <button @click="$store.dispatch('myModule/fetchRoot')">
      Traer los datos del backend
    </button>
    <button @click="$store.dispatch('myModule/fetchRoot', {force: true})">
      Traer los datos de manera forzosa
    </button>
  </div>
</template>
<script lang="ts">
  import {Vue, Component } from "vue-property-decorator";

  @Component({})
  export default class MyComponent extends Vue {

  }
</script>
<style lang="scss" scoped>

</style>
```